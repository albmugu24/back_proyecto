
//Cargo las librerias de testing
const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

//Le decimos a chai que use chaihttp para las peticiones
chai.use(chaihttp);

//Creamos una variable con el resultado que deberia salir de la prueba (expected)
var should = chai.should();

/* PRIMER TEST */

describe('First Test',

  function(){

      //Le mando done a la funcion manejadora de este test
      it('Test that DuckDuckgo works',function(done){

        //Comienzo la peticion (request) al dominio duckduckgo
        //Es el valor que va a tener el actual
        chai.request('http://www.duckduckgo.com')

        //Con esta URL fallaria porque no devolveria 200 (OK)
        //chai.request('https://developer.mozilla.org/en-US/adsfasdfas')

             //Hago una peticion get al raiz de duckduckgo
            .get('/')
            //Cuando haya terminado el get hago lo que pone el end
            .end(

                //El end tiene una funcion manejadora
                function(err,res){

                    //console.log("El request/peticion ha terminado");

                    //Imprimo el error por si tiene alguno, sino va a devolver null
                    //console.log(err);

                    /* AQUI COMIENZO LAS ASERCIONES DEL TEST Y LE DIGO EL VALOR EXPECTED DE CADA ASERCION */

                    //Con esto comprobamos que el res tiene un valor 200
                    res.should.have.status(200);

                    //Lanzo el test porque ya tengo todos los datos que necesito
                    done();
                }
            );
      }
      )
  }
)



/* Test de API TechU */

describe('Test de API TechU',

  function(){

      //Le mando done a la funcion manejadora de este test
      it('Prueba que API TechU Hello responde correctamente',function(done){

        //Comienzo la peticion (request) al dominio localhost
        //Es el valor que va a tener el actual
        chai.request('http://localhost:3000')

             //Hago una peticion al hello de apitechu
            .get('/apitechu/v1/hello')

            //Cuando haya terminado el get hago lo que pone el end
            .end(

                //El end tiene una funcion manejadora
                function(err,res){

                    //console.log("El request/peticion ha terminado");

                    //Imprimo el error por si tiene alguno, sino va a devolver null
                    //console.log(err);

                    /* AQUI COMIENZO LAS ASERCIONES DEL TEST Y LE DIGO EL VALOR EXPECTED DE CADA ASERCION */

                    //Con esto comprobamos que el res tiene un valor 200
                    res.should.have.status(200);

                    //Compruebo que "msg" (declarada en hello) devuelve la cadena "Hola desde API TechU"
                    res.body.msg.should.be.equal("Hola desde API TechU");

                    //Lanzo el test porque ya tengo todos los datos que necesito
                    done();
                }
            );
      }), //AQUI TERMINA EL IT, EL PRIMER TEST

      it('Prueba que API de usuarios devuelve una lista de usuarios correctos',function(done){

        //Comienzo la peticion (request) al dominio localhost
        //Es el valor que va a tener el actual
        chai.request('http://localhost:3000')

             //Hago una peticion al hello de apitechu
            .get('/apitechu/v1/users')

            //Cuando haya terminado el get hago lo que pone el end
            .end(

                //El end tiene una funcion manejadora
                function(err,res){

                    //console.log("El request/peticion ha terminado");

                    //Imprimo el error por si tiene alguno, sino va a devolver null
                    //console.log(err);

                    /* AQUI COMIENZO LAS ASERCIONES DEL TEST Y LE DIGO EL VALOR EXPECTED DE CADA ASERCION */

                    //Con esto comprobamos que lo que devuelve el request (res) tiene un valor 200
                    res.should.have.status(200);

                    //Compruebo que "users" (propiedad de la salida del get) es de tipo array
                    res.body.users.should.be.a('array')

                    //Compruebo que tiene las propiedades esperadas, al menos email y first_name
                    for (user of res.body.users){
                      user.should.have.property('email');
                      user.should.have.property('first_name');
                    }

                    //Lanzo el test porque ya tengo todos los datos que necesito
                    done();
                }
            );
      })

  }
)
