# Imagen raiz de la que partimos (node)
FROM node

# Carpeta raiz de la imagen que vamos a crear
WORKDIR /apitechu

# Copia de archivos (. son todos) de carpeta local a la imagen /apitechu
ADD . /apitechu

# Instalo todas las librerias a la que tengo dependencias (las que estan en package.json)
RUN npm install

# Abrimos el puerto 3000
EXPOSE 3000

# Iniciamos la API mediante "npm start"
# El comando tras el CMD se lanza cuando yo hago un "sudo docker run"
CMD ["npm","start"]
