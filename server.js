
//Cargamos la libreria para manejar las variables de entorno (dotenv) con la configuracion por defecto
//Basicamente lo que haces es cargar la libreria en 'process.env'
require('dotenv').config();

//En la variable express cargamos el framework de express
const express = require('express');

//Iniciamos el framework de express
const app = express();

//Cargamos el modulo io.js (no hace falta poner la extension js)
//En el io.js es donde tenemos la funcion writeUserDataToFile
const io = require('./io')

//Cargamos el controlador UserController en la variable userController
const userController = require ('./controllers/UserController')

//Cargamos el controlador AuthController en la variable authController
const authController = require ('./controllers/AuthController');

//Cargamos el controlador AccountController en la variable accountController
const accountController = require ('./controllers/AccountController');

//Cargamos el controlador AccountController en la variable movementController
const movementController = require ('./controllers/MovementController');

//Con esto permito las peticiones desde el front a cualquier tipo de metodo get/post/delete/put
var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers","Content-Type");

 next();
}

app.use(enableCORS);

//Le decimos a express que parsee los Bodies a JSON
app.use(express.json());

// Creamos el puerto dando preferencia a la variable de entorno y si no existe al puerto 3000
const port = process.env.PORT || 3000;

//Escuchamos al puerto
app.listen(port);
console.log("API escuchando en el puerto " + port);



/*API V2 -> MLAB / MONGODB */

/* POST ACCOUNT -> CREAR UNA CUENTA DADO EL USER E IBAN */

app.post('/apitechu/v2/movement/:iban',movementController.createMovementV2);

/* GET MOVEMENTBYIBAN -> LISTADO DE MOVIMIENTOS DADO UN IBAN */

app.get('/apitechu/v2/movement/:iban',movementController.getMovementByIBANV2);

/* POST ACCOUNT -> CREAR UNA CUENTA DADO EL USER E IBAN */

app.post('/apitechu/v2/account/:userid',accountController.createAccountV2);

/* GET ACCOUNTBYID -> UNA ACCOUNT DADO UN ID */

app.get('/apitechu/v2/account/:id',accountController.getAccountByIdV2);

/* GET USERS -> LISTA COMPLETA */

app.get('/apitechu/v2/users',userController.getUsersV2);

/* GET USERBYID -> UN USUARIO DADO UN ID */

app.get('/apitechu/v2/users/:id',userController.getUserByIdV2);

/* POST USER -> CREAR UN USUARIO */

app.post('/apitechu/v2/users/',userController.createUserV2);

/* LOGIN */

//El LOGIN lo implementamos mediante la funcion loginV2
//Esta funcion la tenemos definido en el controlador authController
//El email y password se pasa en el body de postman

app.post('/apitechu/v2/login',authController.loginUserV2);

/* LOGOUT */

//El LOGOUT lo implementamos mediante la funcion logoutV1
//Esta funcion la tenemos definido en el controlador authController
//Recibe como parametro de entrada el "id" de usuario a borrar
app.delete('/apitechu/v2/logout/:id',authController.logoutUserV2);





















/* API V1 -> FICHERO PLANO */

/* GET USER */

//El GET lo implementamos mediante la funcion getUsersV1
//Esta funcion la tenemos definido en el controlador userController

app.get('/apitechu/v1/users',userController.getUsersV1);


/* POST USER */

//El POST lo implementamos mediante la funcion createUserV1
//Esta funcion la tenemos definido en el controlador userController

app.post('/apitechu/v1/users',userController.createUserV1);

/* DELETE  USER*/

//El DELETE lo implementamos mediante la funcion deleteUserV1
//Esta funcion la tenemos definido en el controlador userController
//Recibe como parametro de entrada el "id" de usuario
app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);

/* LOGIN */

//El LOGIN lo implementamos mediante la funcion loginV1
//Esta funcion la tenemos definido en el controlador authController
//El id y password se pasa en el body de postman

app.post('/apitechu/v1/login',authController.loginUserV1);

/* LOGOUT */

//El LOGOUT lo implementamos mediante la funcion logoutV1
//Esta funcion la tenemos definido en el controlador authController
//Recibe como parametro de entrada el "id" de usuario a borrar
app.delete('/apitechu/v1/logout/:id',authController.logoutUserV1);


/* EJEMPLO HELLO */

//Creamos la primera ruta para la API
app.get('/apitechu/v1/hello',

  function(req,res){

    //Esto me lo va a mostrar en la terminal
    console.log("GET /apitechu/v1/hello");

    //Esta es la respuesta, me lo va a mostrar en Postman
    //Como es un texto, Postman va a interpretar que es html
    //res.send("Hola desde API TechU");

    //Esto interpretaria que es un JSON (sin comillas)
    res.send({"msg":"Hola desde API TechU"});

    //Esto interpretaria que es un HTML (con comillas)
    //res.send('{"msg":"Hola desde API TechU"}');
  }
)

/* EJEMPLO CON TODOS LOS TIPOS DE INPUTS -> MONSTRUO*/

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req,res){

    //Parametros
    console.log("Parametros");
    console.log(req.params);

    //Query String
    console.log("Query String");
    console.log(req.query);

    //Headers
    console.log("Headers");
    console.log(req.headers);

    //Body
    console.log("Body");
    console.log(req.body);
  }
)
