
//Cargamos la libreria bcrypt
const bcrypt = require('bcrypt');

//Creo una funcion para encriptar la info
function hash(data){

  console.log("Hashing data")

  //Llamo a la funcion de encriptacion (hashSync), sincrona (se espera a que termine) y que espera 10 seg
  return bcrypt.hashSync(data,10);
}


//Creo una funcion para comprobar que la password encriptada y sin encriptar estan sincronizadas
//Devuelve un booleano true o false
function checkPassword(passwordFromUserInPlainText,passwordFromDBHashed){

  console.log("Checking password");

  //La funcion compareSync comprueba si el password encriptada y sin encriptar estan sincronizadas
  return bcrypt.compareSync(passwordFromUserInPlainText,passwordFromDBHashed)
}


module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
