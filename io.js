

const fs = require('fs');

function writeUserDataToFile(data){
console.log("writeUserDataToFile");

var jsonUserData = JSON.stringify(data);

     console.log("Estoy aqui");

fs.writeFile("./usuarios.json", jsonUserData, "utf8",
   function(err) {
     if (err){
       console.log(err);
     }else{
       console.log("Datos de usuario escritos");
     }
   }
);
}
module.exports.writeUserDataToFile = writeUserDataToFile;

/*

//Creamos una funcion para escribir datos en fichero
//La podemos usar tanto en el POST como DELETE
function writeUserDataToFile(users){

  console.log("writeUserDataToFile");

  //Cargo la libreria fs
  const fs = require ('fs');

  //Me creo una variable con lo que quiero guardar y lo transformo a string
  var jsonUserData = JSON.stringify(users);

  //Guardo el archivo en un fichero
  fs.writeFile("./usuarios.json",jsonUserData,"utf8",
   //Cuarto parametro es la funcion manejadora de gestion errores
    function(err){
      if(err){
        console.log(err);
      }else{
        console.log("Datos de usuario escritos");
      }
    }
  )
}

//Con esto lo que hacemos es exponer la funcion writeUserDataToFile
//para poder referenciarla en otros archivos
module.exports.writeUserDataToFile = writeUserDataToFile;

*/
