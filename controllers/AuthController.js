
/* API V2 -> MLAB */

//Cargamos la libreria request-json
const requestJson = require ('request-json');

//Cargamos la libreria crypt para encriptar contraseñas
const crypt = require ('../crypt');

//Creamos la variable para ir construyendo la URL
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuamg9ed/collections/";

//Creamos una variable con la APIKEY declarada como variable de entorno (MLAB_API_KEY) en el archivo .env
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

/* LOGIN V2 */

function loginUserV2(req,res){

    console.log('LOGIN /apitechu/v2/login');

    //Saco los datos que me vienen del body de Postman;
    console.log("Email: " + req.body.email);
    console.log("Password: " + req.body.password);

    var email = req.body.email;
    var passwordInput = req.body.password;

    //Creo una variable query con la consulta a incluir en la URL
    // Lo que quiero poner es -> q={"email":"gbremmell5@ameblo.jp"}
    var query = 'q={"email":"' + email + '"}'

    console.log(query);


    //Creamos una conexion con la URL de MLAB
    var httpClient = requestJson.createClient(baseMlabURL);

    //Hacemos una peticion "get" para verificar si el usuario existe
    //Si no existe, devuelve error
    httpClient.get("user?" + query + "&" + mLabAPIKEY,

      //funcion manejadora
      function(err,resMLab,body){

        //si hay error, devuelvo un mensaje y error 500
        if (err){
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          //Devuelvo estado 500 -> error del servidor (Internal Server Error)
          res.status(500);

        }else{

          //Si no hay error y he encontrato el usuario (body tiene tamaño > 0) devuelvo el body
          //El body es un trozo parcial parseado a json de la respuesta cruda de MLAB (resMLab)
          if (body.length > 0){

            //Devolvemos el body (que tiene formato object) pero en formato json (el primer elemento del object)
            //Si no pongo [0] me devuelve un object []
            var user = body[0];
            var passwordBD = body[0].password;
            //var passwordOK = crypt.checkPassword (passwordBD,passwordInput)

            var idBD = body[0].id;

            //El estado 200 lo devuelve por defecto si no hay fallo, no hay que ponerlo
            //Solo hago el get ha encontrado el usuario (estado 200)
             if (res.status(200)){

               console.log ("Encontrado usuario " + email);

               //Si la contraseña coincide hago la peticion PUT
                if(passwordInput == passwordBD){ //if (passwordOK)

                  console.log ("La password coincide");


                  //creo una variable para añadir la propiedad logged a true
                  var putBody = '{"$set":{"logged":true}}';

                 //Hago la peticion put, incluyendo la query (filtro por email el usuario que logear)
                 //El segundo parametro del put es el putBody (la propiedad que quiero escribir) casteado a JSON (lo hemos creado como String)
                  httpClient.put("user?" + query  + "&"  + mLabAPIKEY, JSON.parse(putBody),

                    //funcion manejadora
                    //Ojo cambiar los nombres de los parametros funcion manejadora para que no se pise
                    function(errPUT,resMLabPUT,bodyPUT){

                      //si hay error, devuelvo un mensaje y error 500
                      if (err){
                        var response = {
                          "msg" : "Error logeando usuario"
                        }
                        //Devuelvo estado 500 -> error del servidor (Internal Server Error)
                        res.status(500);
                      }
                    }
                  )
                  //Informe mensaje de usuario logeado correctamente
                  var response = {
                    "msg" : "Usuario logeado",
                    "id" : idBD
                  }
                }

                //Si no coincide la password, devuelvo password incorrecto e informo error 401 (no autorizado)
                else {
                  var response = {
                    "msg" : "Password incorrecto",
                    "id" : idBD
                  }
                  res.status(401);
                }
             }

          //Si no hay error pero no he encontrado el usuario (body tamaño 0), devuelvo mensaje y error 404
          }else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            //Devuelvo el estado 404 -> Not found
            res.status(404)
          }
        }

        //Devuelvo la salida
        res.send(response);
        console.log(response);
      }
    )
}


/* LOGOUT V2 */

function logoutUserV2(req, res) {

    console.log("LOGOUT /apitechu/v2/logout/:id");

    //Saco los datos que me vienen de los parametros del postman
    console.log("Id a logout: " + req.params.id);

    //Creo una variable query con la consulta a incluir en la URL
    // Lo que quiero poner es -> q={"id":6}
    var query = 'q={"id":' + req.params.id + '}'
    console.log(query);


    //Creamos una conexion con la URL de MLAB
    var httpClient = requestJson.createClient(baseMlabURL);

    //Hacemos una peticion "get" para verificar si el usuario existe
    //Si no existe, devuelve error
    httpClient.get("user?" + query + "&" + mLabAPIKEY,

      //funcion manejadora
      function(err,resMLab,body){

        //si hay error, devuelvo un mensaje y error 500
        if (err){
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          //Devuelvo estado 500 -> error del servidor (Internal Server Error)
          res.status(500);

        }else{

          //Si no hay error y he encontrato el usuario (body tiene tamaño > 0) devuelvo el body
          //El body es un trozo parcial parseado a json de la respuesta cruda de MLAB (resMLab)
          if (body.length > 0){

            //Devolvemos el body (que tiene formato object) pero en formato json (el primer elemento del object)
            //Si no pongo [0] me devuelve un object []
            var user = body[0]

            //Solo hago el get ha encontrado el usuario (estado 200)
             if (res.status(200)){

               //Si esta logeado
                if(user.logged){

                  console.log ("Encontrado usuario logeado con id " + user.id);


                  //creo una variable para eliminar la propiedad logged a true
                  var putBody = '{"$unset":{"logged":""}}';

                 //Hago la peticion put, incluyendo la query (filtro por id del usuario a deslogear)
                 //El segundo parametro del put es el putBody (la propiedad que quiero borrar) casteado a JSON (lo hemos creado como String)
                  httpClient.put("user?" + query  + "&"  + mLabAPIKEY, JSON.parse(putBody),

                    //funcion manejadora
                    function(err,resMLab,body){
                      //si hay error, devuelvo un mensaje y error 500
                      if (err){
                        var response = {
                          "msg" : "Error deslogeando usuario"
                        }
                        //Devuelvo estado 500 -> error del servidor (Internal Server Error)
                        res.status(500);
                      }
                    }
                  )

                //Informo mensaje de deslogeo OK
                  var response = {
                    "msg" : "Usuario deslogeado correctamente",
                    "id" : user.id
                  }
                }

                //Si no está logeado
                else {
                  var response = {
                    "msg" : "Usuario no logeado",
                    "id" : user.id
                  }
                  res.status(401);
                }
             }

          //Si no hay error pero no he encontrado el usuario (body tamaño 0), devuelvo mensaje y error 404
          }else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            //Devuelvo el estado 404 -> Not found
            res.status(404)
          }
        }

        //Devuelvo la salida
        res.send(response);
        console.log(response);
      }
    )

}

//Con esto lo que hacemos es exponer las funciones del controlador
//para poder referenciarla en otros archivos
module.exports.loginUserV2 = loginUserV2;
module.exports.logoutUserV2 = logoutUserV2;
















/* API V1 -> FICHERO PLANO */

/* LOGIN */

//Cargamos el modulo io.js (no hace falta poner la extension js)
//En el io.js es donde tenemos la funcion writeUserDataToFile
//Lo vamos a usar en el LOGIN y LOGOUT
//El archivo io.js está en la carpeta padre (..)
const io = require('../io');

//Implementamos la funcion loginUserV1 para logear un usuario (POST)
function loginUserV1(req,res){

    console.log('LOGIN /apitechu/v1/login');

    //Boooleano para indicar si ha logeado o no
    var logged = false;

    //Cargo el archivo de usuarios
    //Como estamos en la carpeta controllers, tenemos que buscar el archivo en la carpeta padre (..)
    var users = require('../usuarios.json');

    //Saco los datos que me vienen del body de Postman;
    console.log("Email: " + req.body.email);
    console.log("Password: " + req.body.password);

    //Recorro la longitud del array con un iterador
    for (var i = 0; i < users.length; i++) {

       console.log("comparando " + users[i].email + " y " +  req.body.email);
       console.log("comparando " + users[i].password + " y " +  req.body.password);

       //Si coinciden usuario y password y no está logeado
       if (users[i].email == req.body.email && users[i].password == req.body.password && !users[i].logged) {

        console.log("El email " + users[i].email + " coincide");
        console.log("La password " + users[i].password + " coincide");

        //Añado la propieda logged a true
        users[i].logged=true;

        //Marco el booleano como logeado
        logged = true;

        //Me guardo el id para luego devolverlo
        var id = users[i].id;

        //Lo escribo en el fichero
        io.writeUserDataToFile(users);

        //Paro para no seguir iterando
        break;
      }
    }

    //Si ha logeado algo, lo añado al fichero
    //if (logged) {
     //console.log(users);
     //io.writeUserDataToFile(users);
    //}

    //Informo una cadena informando si se ha logeado o no
    var msg = logged ?
    "Login correcto" : "Login incorrecto"


    //Informo la salida en funcion si esta logeado (con el id) o no (incorrecto)
    var salida = logged ?
     {"mensaje" : msg, "idUsuario" : id} : {"mensaje" : msg}


     //Escribo es la salida en el terminal
    //  console.log(salida);

     //Devuelvo la salida
      res.send(salida);


}

/* DELETE */

//Implementamos la funcion deleteUserV1 para borrar un usuario (DELETE)
function logoutUserV1(req, res) {

  console.log("LOGOUT /apitechu/v1/logout/:id");

  //Booleano para indicar si es deslogeado o no
  var logout = false;

  //Cargo el archivo de usuarios
  //Como estamos en la carpeta controllers, tenemos que buscar el archivo en la carpeta padre (..)
  var users = require('../usuarios.json');

  //Saco los datos que me vienen de los parametros del postman
  console.log("Id a logout: " + req.params.id);

  //Recorro la longitud del array con un iterador
  for (var i = 0; i < users.length; i++) {

     console.log("comparando id's " + users[i].id + " y " +  req.params.id);

     //Si coincide el id
     if (users[i].id == req.params.id && users[i].logged) {

      console.log("El id " + users[i].id + " coincide y está logeado");

      //Borro la propiedad logged
      delete users[i].logged;

      //Marco el booleano como deslogueado
      logout = true;

      //Me guardo el id para luego devolverlo
      var id =   users[i].id;

      //Lo escribo en fichero
      io.writeUserDataToFile(users);

      //Paro para no seguir iterando
      break;

    }
  }

  //Si ha desogeado algo, lo añado al fichero
  //if (logout) {
    //console.log(users);
    //io.writeUserDataToFile(users);
  //}

  //Informo una cadena de salida en funcion de si ha borrado o no
  var mensaje = logout ?
  "Logout correcto" : "Logout incorrecto"


  //Informo la salida en funcion si esta logeado o no
  var output = logout ?
   {"mensaje" : mensaje, "idUsuario" : id} : {"mensaje" : mensaje}


   //Escribo es la salida en el terminal
    //console.log(output);

   //Devuelvo la salida
    res.send(output);

}

//Con esto lo que hacemos es exponer las funciones del controlador
//para poder referenciarla en otros archivos
module.exports.loginUserV1 = loginUserV1;
module.exports.logoutUserV1 = logoutUserV1;
