
/* API V2 */

function loginV2(req, res) {
 console.log("POST /apitechu/v2/login");

 var query = 'q={"email": "' + req.body.email + '"}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     var isPasswordcorrect =
       crypt.checkPassword(req.body.password, body[0].password);
     console.log("Password correct is " + isPasswordcorrect);

     if (!isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.status(401);
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}



function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}



/* API V1 */

const io = require('../io');

function loginV1(req, res) {
 console.log("POST /apitechu/v1/login");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.email == req.body.email
       && user.password == req.body.password) {
     console.log("Email found, password ok");
     var loggedUserId = user.id;
     user.logged = true;
     console.log("Logged in user with id " + user.id);
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedUserId ?
   "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedUserId
 };

 res.send(response);
}

function logoutV1(req, res) {
 console.log("POST /apitechu/v1/logout");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.id == req.params.id && user.logged === true) {
     console.log("User found, logging out");
     delete user.logged
     console.log("Logged out user with id " + user.id);
     var loggedoutUserId = user.id;
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedoutUserId ?
   "Logout correcto" : "Logout incorrecto";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedoutUserId
 };

 res.send(response);
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
