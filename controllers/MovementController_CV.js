/* API VERSION2 -> MLAB / MONGODB */

//Cargamos la libreria request-json
const requestJson = require ('request-json');

//Creamos la variable para ir construyendo la URL
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuamg9ed/collections/";

//Creamos una variable con la APIKEY declarada como variable de entorno (MLAB_API_KEY) en el archivo .env
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

//Creamos la variable para ir construyendo la URL de la API de Contravaloracion (no tiene API_KEY)
const baseCurrencyConvURL = "https://free.currencyconverterapi.com/api/v6/";


/* GET DE MOVEMENTS's DADO UNA CUENTA/IBAN */

function getMovementByIBANV2(req,res){

  console.log('GET /apitechu/v2/movement/:iban');


  //Saco los datos que me vienen de los parametros del postman
  console.log("El iban cuyos movimientos quiero devolver es: " + req.params.iban);
  var iban = req.params.iban;

  //Creo una variable query con la consulta a incluir en la URL
  // Lo que quiero poner es -> q={"iban":"MU08 LGYE 8407 5513 4707 8424 183A DC"}
  var query = 'q={"iban":"' + iban + '"}';
  console.log(query);

  //Creamos una conexion con la URL de MLAB
  var httpClient = requestJson.createClient(baseMlabURL);

  //Hacemos una peticion "get" al cliente completando el resto de la URL
  //Ojo que cuando hay mas parametros hay que incluir el & (aspersan)
  httpClient.get("movement?" + query + "&" + mLabAPIKEY,

    //funcion manejadora
    function(err,resMLab,body){

      //si hay error, devuelvo un mensaje y error 500
      if (err){
        var response = {
          "msg" : "Error obteniendo movimientos"
        }
        //Devuelvo estado 500 -> error del servidor (Internal Server Error)
        res.status(500);

      }else{

        //Si no hay error y he encontrato el usuario (body tiene tamaño > 0) devuelvo el body
        //El body es un trozo parcial parseado a json de la respuesta cruda de MLAB (resMLab)
        if (body.length > 0){

          //Devolvemos el body (que tiene formato object) pero en formato json (el primer elemento del object)
          var response = body;


          //El estado 200 lo devuelve por defecto si no hay fallo, no hay que ponerlo

        //Si no hay error pero no he encontrado el usuario (body tamaño 0), devuelvo mensaje y error 404
        }else {
          var response = {
            "msg" : "Esta cuenta no tiene movimientos"
          }
          //Devuelvo el estado 404 -> Not found
          res.status(404)
        }
      }
      //Devuelvo la salida
      res.send(response);
    }
  )

}

module.exports.getMovementByIBANV2 = getMovementByIBANV2;







/* POST MOVEMENT -> CREATE MOVEMENT */

function createMovementV2(req,res){

    console.log('POST /apitechu/v2/movement/:iban');

    //Creamos una variable para definir si es un ingreso o reintegro
    var type = req.body.amount>=0 ?
    "Ingreso" : "Retirada";

    //Codigo para obtener la fecha de hoy
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10) {
      dd = '0' + dd;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }

    today = yyyy + '-' + mm + '-' + dd;

    //Saco los datos que me vienen del body de Postman;
    console.log("IBAN es: " + req.params.iban);
    console.log("Importe es: " + req.body.amount);
    console.log("Moneda es: " + req.body.currency);
    console.log("Concepto es: " + req.body.concept);
    console.log("Operacion de tipo:" + type);
    console.log("Fecha Movimiento es: " + today);

    //Query para comprobar si la cuenta (IBAN) existe
    var query = 'q={"iban":"' + req.params.iban + '"}'
    console.log(query);

    //Creamos una conexion con la URL de MLAB
    var httpClient = requestJson.createClient(baseMlabURL);

    //Creamos una conexion con la API para contravalorar
    var httpClientCurrencyConv = requestJson.createClient(baseCurrencyConvURL);

    //Hacemos una peticion "get" para verificar si el IBAN existe
    //Si no existe, devuelve error
    httpClient.get("account?" + query + "&" + mLabAPIKEY,

      //funcion manejadora
      function(err,resMLab,body){

        //si hay error, devuelvo un mensaje y error 500
        if (err){
          var response = {
            "msg" : "Error obteniendo la cuenta"
          }
          //Devuelvo estado 500 -> error del servidor (Internal Server Error)
          res.status(500);

        }else{

          //Si la cuenta existe (body > 0)
          //El body es un trozo parcial parseado a json de la respuesta cruda de MLAB (resMLab)
          if (body.length > 0){

            //Creamos la query para contravalorar
            //Por ejemplo "q=USD_EUR"
            var queryCurrencyConv = 'q=' + req.body.currency + '_EUR'
            console.log(queryCurrencyConv);

            //Hacemos una peticion "get" para verificar si el IBAN ya existe
            //Si no existe, devuelve error
            httpClientCurrencyConv.get("convert?" + queryCurrencyConv + "&compact=y",

              //funcion manejadora
              function(errConv,resConv,bodyConv){

                console.log("Entro en la contravaloracion");
                console.log(bodyConv);

                //si hay error, devuelvo un mensaje y error 500
                if (errConv){
                  var response = {
                    "msg" : "Error contravalorando el importe"
                  }
                  //Devuelvo estado 500 -> error del servidor (Internal Server Error)
                  res.status(500);

                }else{

                  //Obtengo la tasa de cambio a partir de la respuesta de la API
                  var tasa_cambio = bodyConv[Object.keys(bodyConv)[0]].val
                  console.log(tasa_cambio);

                  //Contravaloro el importe a EUR
                  var amountEUR = parseFloat((req.body.amount * tasa_cambio).toFixed(2));
                  console.log(amountEUR);

                  //Cargo en un objeto newMovement cada uno de los datos
                  var newMovement = {
                    "iban":req.params.iban,
                    "amount":req.body.amount,
                    "currency":req.body.currency,
                    "amountEUR":amountEUR,
                    "concept":req.body.concept,
                    "type":type,
                    "date":today
                  }

                  console.log(newMovement);

                  //Hago la peticion POST para crear la cuenta
                  //El segundo parametro del POST es el JSON con la cuenta que quiero crear
                   httpClient.post("movement/?" + mLabAPIKEY, newMovement,

                     //funcion manejadora
                     //Ojo cambiar los nombres de los parametros funcion manejadora para que no se pise
                     function(errPOST,resMLabPOST,bodyPOST){

                       console.log("Insertando el nuevo movimiento")

                       //si hay error, devuelvo un mensaje y error 500
                       if (errPOST){
                        console.log("Ha fallado la creacion del movimiento")
                         var response = {
                           "msg" : "Error creando movimiento"
                         }
                         //Devuelvo estado 500 -> error del servidor (Internal Server Error)
                         res.status(500);
                       }

                       else{

                       //Si el movimiento se ha creado correctamente
                         if (body.length > 0){

                           // Creo la query para modificar el balance de la cuenta
                           // Por ejemplo -> { $inc: { balance: -2,balanceEUR: -2.1} }
                             var sumaBalance = '{"$inc":{"balance":' + req.body.amount + ', "balanceEUR":' + amountEUR +'}}'
                             console.log(sumaBalance);

                             //Hago la peticion put, incluyendo la query (filtro por cuenta cuyo balance quiero actualizar)
                             //El segundo parametro del put es el sumaBalance (la propiedad que quiero updatear) casteado a JSON (lo hemos creado como String)
                             httpClient.put("account?" + query  + "&"  + mLabAPIKEY, JSON.parse(sumaBalance),

                               //funcion manejadora
                               //Ojo cambiar los nombres de los parametros funcion manejadora para que no se pise
                               function(errPUT,resMLabPUT,bodyPUT){

                                 console.log("Actualizando el balance de la cuenta")

                                 //si hay error, devuelvo un mensaje y error 500
                                 if (errPUT){
                                   console.log("Ha fallado la actualizacion del balance")
                                   var response = {
                                     "msg" : "Error actualizando balance de la cuenta"
                                   }
                                   //Devuelvo estado 500 -> error del servidor (Internal Server Error)
                                   res.status(500);
                                 }
                               }
                             )
                         }
                       }
                     }
                   )
                }
              }
            )
            //Informe mensaje de movimiento creado OK
            var response = {
              "msg" : "Movimiento creado correctamente y balance de la cuenta actualizado",
            }
          }

          //Si la cuenta no existe (body = 0)
          else{

            var response = {
                "msg" : "Esta cuenta no existe",
            }
          res.status(401);
          }

        //Devuelvo la salida
        res.send(response);
        console.log(response);
      }
    }
  )

}



module.exports.createMovementV2 = createMovementV2;
