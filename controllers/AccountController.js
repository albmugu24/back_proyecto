/* API VERSION2 -> MLAB / MONGODB */

//Cargamos la libreria request-json
const requestJson = require ('request-json');

//Creamos la variable para ir construyendo la URL
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuamg9ed/collections/";

//Creamos una variable con la APIKEY declarada como variable de entorno (MLAB_API_KEY) en el archivo .env
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

//Creamos la variable para ir construyendo la URL de la API de Contravaloracion (no tiene API_KEY)
const baseCurrencyConvURL = "https://ratesapi.io/api/";


/* GET DE ACCOUNT's DADO UN ID DE USUARIO */

function getAccountByIdV2(req,res){

  console.log('GET /apitechu/v2/account/:id');


  //Saco los datos que me vienen de los parametros del postman
  console.log("Id de usuario cuyas cuentas quiero devolver: " + req.params.id);
  var id = req.params.id;

  //Creo una variable query con la consulta a incluir en la URL
  // Lo que quiero poner es -> q={"userid":3}
  var query = 'q={"userid":' + id + '}';

  //Creamos una conexion con la URL de MLAB
  var httpClient = requestJson.createClient(baseMlabURL);

  //Hacemos una peticion "get" al cliente completando el resto de la URL
  //Ojo que cuando hay mas parametros hay que incluir el & (aspersan)
  httpClient.get("account?" + query + "&" + mLabAPIKEY,

    //funcion manejadora
    function(err,resMLab,body){

      //si hay error, devuelvo un mensaje y error 500
      if (err){
        var response = {
          "msg" : "Error obteniendo cuenta"
        }
        //Devuelvo estado 500 -> error del servidor (Internal Server Error)
        res.status(500);

      }else{

        //Si no hay error y he encontrato el usuario (body tiene tamaño > 0) devuelvo el body
        //El body es un trozo parcial parseado a json de la respuesta cruda de MLAB (resMLab)
        if (body.length > 0){

          //Devolvemos el body (que tiene formato object) pero en formato json (el primer elemento del object)
          var response = body;

        //Si no hay error pero no he encontrado el usuario (body tamaño 0), devuelvo mensaje y error 404
        }else {
          var response = {
            "msg" : "Este usuario no tiene cuentas"
          }
          //Devuelvo el estado 404 -> Not found
          res.status(404)
        }
      }
      //Devuelvo la salida
      res.send(response);
    }
  )

}

module.exports.getAccountByIdV2 = getAccountByIdV2;



/* POST -> CREATE ACCOUNT */

function createAccountV2(req,res){

    console.log('POST /apitechu/v2/account/:userid');

    //Saco los datos que me vienen del body de Postman;
    console.log("IBAN es: " + req.body.iban);
    console.log("Moneda es: " + req.body.currency);
    console.log("Id Usuario es: " + req.params.userid);

    //Casteamos el userid de string a number
    var userid = Number(req.params.userid);

    //Query para comprobar si el IBAN ya existe
    var query = 'q={"iban":"' + req.body.iban + '"}'
    console.log(query);

    //Codigo para obtener la fecha de hoy
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10) {
      dd = '0' + dd;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }

    today = yyyy + '-' + mm + '-' + dd;

    //Creamos una conexion con la URL de MLAB
    var httpClient = requestJson.createClient(baseMlabURL);

    //Creamos una conexion con la API para contravalorar
    var httpClientCurrencyConv = requestJson.createClient(baseCurrencyConvURL);

    //Hacemos una peticion "get" para verificar si el IBAN ya existe
    //Si no existe, devuelve error
    httpClient.get("account?" + query + "&" + mLabAPIKEY,

      //funcion manejadora
      function(err,resMLab,body){

        //si hay error, devuelvo un mensaje y error 500
        if (err){
          var response = {
            "msg" : "Error obteniendo las cuentas"
          }
          //Devuelvo estado 500 -> error del servidor (Internal Server Error)
          res.status(500);

        }else{

          //Si la cuenta no existe (body = 0)
          //El body es un trozo parcial parseado a json de la respuesta cruda de MLAB (resMLab)
          if (body.length == 0){

            //Creamos la query para contravalorar
            //Por ejemplo "base=EUR&symbols=USD"
            var queryCurrencyConv = 'base=EUR&symbols=' + req.body.currency
            console.log(queryCurrencyConv);

            //Hacemos la peticion get para contravalorar
            httpClientCurrencyConv.get("latest?" + queryCurrencyConv,

              //funcion manejadora
              function(errConv,resConv,bodyConv){

                console.log("Entro en la contravaloracion")
                console.log(bodyConv);

                //si hay error, devuelvo un mensaje y error 500
                if (errConv){
                  console.log("He fallado ");
                  var response = {
                    "msg" : "Error contravalorando el importe"
                  }
                  //Devuelvo estado 500 -> error del servidor (Internal Server Error)
                  res.status(500);

                }else{

                    //Creamos la cuenta con 0 euros de regalo
                    var impEUR = 0
                    var regaloEUR = impEUR;
                    console.log(regaloEUR);

                    //Obtengo la tasa de cambio a partir de la respuesta de la API
                    var tasa_cambio_aux = bodyConv[Object.keys(bodyConv)[2]];
                    var tasa_cambio = tasa_cambio_aux[Object.keys(tasa_cambio_aux)[0]];
                    console.log(tasa_cambio);

                    //Contravaloro el importe a DIV dado los 100 de regalo en EUR
                    if (req.body.currency=="EUR"){
                      regaloDIV=impEUR;
                    }
                    else{
                      var regaloDIV = parseInt((regaloEUR * tasa_cambio).toFixed(0));
                    }

                    console.log(regaloDIV);

                    //Cargo en un objeto newAccount cada uno de los datos
                    var newAccount = {
                      "userid":userid,
                      "iban":req.body.iban,
                      "balance":regaloDIV,
                      "currency":req.body.currency,
                      "balanceEUR":regaloEUR
                    }

                    console.log(newAccount);

                    //Hago la peticion POST para crear la cuenta
                    //El segundo parametro del POST es el JSON con la cuenta que quiero crear
                     httpClient.post("account/?" + mLabAPIKEY, newAccount,

                         //funcion manejadora
                         //Ojo cambiar los nombres de los parametros funcion manejadora para que no se pise
                         function(errPOST,resMLabPOST,bodyPOST){

                           //si hay error, devuelvo un mensaje y error 500
                           if (errPOST){
                             var response = {
                               "msg" : "Error creando cuenta"
                             }
                             //Devuelvo estado 500 -> error del servidor (Internal Server Error)
                             res.status(500);
                           }
                         }
                       )
                    }
              }
            )
            //Informe mensaje de cuenta creada correctamente
            var response = {
              "msg" : "Cuenta creada correctamente",
            }

          }

          //Si la cuenta ya existe (body > 0)
          else{

            var response = {
                "msg" : "Esta cuenta ya existe",
            }
          res.status(401);
          }

        //Devuelvo la salida
        res.send(response);
        console.log(response);
      }
    }
  )
}



module.exports.createAccountV2 = createAccountV2;
