/* API VERSION2 -> MLAB / MONGODB */

//Cargamos la libreria request-json
const requestJson = require ('request-json');

//Cargamos la libreria crypt para encriptar contraseñas
const crypt = require ('../crypt');

//Creamos la variable para ir construyendo la URL
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuamg9ed/collections/";

//Creamos una variable con la APIKEY declarada como variable de entorno (MLAB_API_KEY) en el archivo .env
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;


/* GET de la LISTA COMPLETA DE USUARIOS */

function getUsersV2(req,res){

  console.log('GET /apitechu/v2/users');

  //Creamos una conexion con la URL de MLAB
  var httpClient = requestJson.createClient(baseMlabURL);

  //Hacemos una peticion "get" al cliente completando el resto de la URL
  httpClient.get("user?" + mLabAPIKEY,

    //funcion manejadora
    function(err,resMLab,body){

      //Si no hay error devuelvo el body, si hay error, devuelvo un mensaje
      //El body es un trozo parcial parseado a json de la respuesta cruda de MLAB (resMLab)
      var response = !err ? body : {
        "msg" : "Error obteniendo usuario"
      }

      //Devuelvo la salida
      res.send(response);
    }
  )
}

module.exports.getUsersV2 = getUsersV2;

/* GET DE UN USUARIO DADO UN ID DE USUARIO */

function getUserByIdV2(req,res){

  console.log('GET /apitechu/v2/users/:id');

  //Saco los datos que me vienen de los parametros del postman
  console.log("Id a devolver: " + req.params.id);
  var id = req.params.id;

  //Creo una variable query con la consulta a incluir en la URL
  // Lo que quiero poner es -> q={"id":3}
  var query = 'q={"id":' + id + '}';

  //Creamos una conexion con la URL de MLAB
  var httpClient = requestJson.createClient(baseMlabURL);

  //Hacemos una peticion "get" al cliente completando el resto de la URL
  //Ojo que cuando hay mas parametros hay que incluir el & (aspersan)
  httpClient.get("user?" + query + "&" + mLabAPIKEY,

    //funcion manejadora
    function(err,resMLab,body){

      //si hay error, devuelvo un mensaje y error 500
      if (err){
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        //Devuelvo estado 500 -> error del servidor (Internal Server Error)
        res.status(500);

      }else{

        //Si no hay error y he encontrato el usuario (body tiene tamaño > 0) devuelvo el body
        //El body es un trozo parcial parseado a json de la respuesta cruda de MLAB (resMLab)
        if (body.length > 0){

          //Devolvemos el body (que tiene formato object) pero en formato json (el primer elemento del object)
          //Si no pongo [0] me devuelve un object []
          var response = body[0];


          //El estado 200 lo devuelve por defecto si no hay fallo, no hay que ponerlo

        //Si no hay error pero no he encontrado el usuario (body tamaño 0), devuelvo mensaje y error 404
        }else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          //Devuelvo el estado 404 -> Not found
          res.status(404)
        }
      }
      //Devuelvo la salida
      res.send(response);
    }
  )

}

module.exports.getUserByIdV2 = getUserByIdV2;


/* CREATE USER V2 */

function createUserV2(req,res){

    console.log('POST /apitechu/v2/users');

    console.log(req.body);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    var httpClient = requestJson.createClient(baseMlabURL);

     //Hago una consulta para obtener el ultimo id hasta ahora
     var query = 'f={"id":1, "_id":0}&s={"id":-1}&l=1';
     console.log(query);

     var ultID;
     var newID;

     //Hago la petición get para obtener est ultimo id
     httpClient.get("user/?" + query + "&" + mLabAPIKEY,

       //Funcion manejadora
        function(err, resMLab, body){

          //si hay error, devuelvo un mensaje y error 500
          if (err){
            var response = {
              "msg" : "Error obteniendo usuario"
            }
            //Devuelvo estado 500 -> error del servidor (Internal Server Error)
            res.status(500);

          }else{

            //Si no hay error y he encontrato el usuario (body tiene tamaño > 0) devuelvo el body
            //El body es un trozo parcial parseado a json de la respuesta cruda de MLAB (resMLab)
            if(body.length > 0){

              var response = body[0];
              ultID = response.id;
              console.log("El ultimo ID encontrado es:" + ultID);

              newID = ultID + 1;
              console.log("El ID del proximo usuario es: "+ newID);

            var newUser = {
              "id" : newID,
              "first_name": req.body.first_name,
              "last_name": req.body.last_name,
              "email": req.body.email,
              "password" : req.body.password
            }

            //Hago la petición POST para crear el usuario
            httpClient.post("user/?" + mLabAPIKEY, newUser,

             //Funcion manejadora
              function(err, resMLab, body){

                console.log("Usuario creado correctamente");

                //Devolvemos estado 201 -> Elemento creado
                res.status(201);

                //Devuelvo la salida
                res.send({"msg":"Usuario creado correctamente"})

              }
            )

            //Si no hay error pero no he encontrado el usuario (body tamaño 0), devuelvo mensaje y error 404
            }else{

              //Informo usuario no encontrado
              var response = {
                "msg" : "Usuario no encontrado"
              }
              //Devuelvo el estado 404 -> Not found
              res.status(404)
            }
          }

          newID = ultID + 1;
        }
      );
}

module.exports.createUserV2 = createUserV2;

/* DELETE USER V2 */


























/* API VERSION 1 -> FICHERO PLANO */

/* GET */

//Implementamos la funcion getUsersV1 para devolver la lista de usuarios (GET)
//Ademas le añadimos la opcion top y count

function getUsersV1(req,res){

  console.log('GET /apitechu/v1/users');


  //Cargo el archivo de usuarios
  //Como estamos en la carpeta controllers, tenemos que buscar el archivo en la carpeta padre (..)
  var users = require('../usuarios.json');

  //Creo la variable vacia del resultado que voy a devolver
  var result = {};

  //Si hay count
  if(req.query.$count == "true"){

    console.log("Hay un count");

    //Añado el count al resultado
    result.count = users.length;
  }

  //Si me piden el top
  if(req.query.$top){

    console.log("Hay un top")

    //Añado a la lista de usuarios solo hasta el top
    result.users = users.slice(0,req.query.$top);
  }

  //Siempre devuelvo toda la lista de usuarios salvo que me pidan top
  //Es decir, el count devuelve el count y la lista de usuarios
  else {
    console.log("Hola");
    result.users = users;
  }

  //Devuelvo el resultado
  res.send(result);

}

/* POST */

//Cargamos el modulo io.js (no hace falta poner la extension js)
//En el io.js es donde tenemos la funcion writeUserDataToFile
//Lo vamos a usar en el POST y el DELETE
//El archivo io.js está en la carpeta padre (..)
const io = require('../io');

//Implementamos la funcion createUserV1 para crear un usuario nuevo (POST)
function createUserV1(req,res){

    console.log('POST /apitechu/v1/users');

    //Saco los datos que me vienen del body de Postman;
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    //Con esto sacaria el body entero
    //console.log(req.body);


    //Cargo en un objeto newUser cada uno de los datos
    var newUser = {
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email
    }

    //Cargo en un array el fichero de usuarios que ya existe
    //Como estamos en la carpeta controllers, tenemos que buscar el archivo en la carpeta padre (..)
    var users = require('../usuarios.json')

    //Añado el nuevo usuario al array
    users.push(newUser);

    //Llamo a la funcion que escribe el array users en el fichero
    //Ver funcion en io.js
    io.writeUserDataToFile(users);

    console.log("Usuario añadido");

}

/* DELETE */

//Implementamos la funcion deleteUserV1 para borrar un usuario nuevo (POST)
function deleteUserV1(req, res) {

  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  //Cargamos en un array el fichero de usuarios
  var users = require('../usuarios.json');

  //Inicializo un booleano que indica si borrado o no
  var deleted = false;

  /* FOR NORMAL */

  console.log("Usando for normal");

  //Recorro la longitud del array con un iterador
  for (var i = 0; i < users.length; i++) {

     console.log("comparando " + users[i].id + " y " +  req.params.id);

     //Si el id de la posicion "i" es igual al id pasado por parametro
     if (users[i].id == req.params.id) {

      console.log("La posicion " + i + " coincide");

      //Borro la posicion id y marco como borrado
      users.splice(i, 1);
      deleted = true;

      //Paro para no seguir iterando
      break;
    }
  }

  //Si ha borrado algo, escribo el array borrado en el fichero
  if (deleted) {
    io.writeUserDataToFile(users);
  }

  //Informo una cadena de salida en funcion de si ha borrado o no
  var msg = deleted ?
  "Usuario borrado" : "Usuario no encontrado."

 //Escribo es la salida en el terminal
  console.log(msg);

  //Devuelvo la salida por Postman
  res.send({"msg" : msg});

  // console.log("Usando for in");
  // for (arrayId in users) {
  //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
  //   if (users[arrayId].id == req.params.id) {
  //     console.log("La posicion " + arrayId " coincide");
  //     users.splice(arrayId, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of");
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición ? coincide");
  //     // Which one to delete? order is not guaranteed...
  //     deleted = false;
  //     break;
  //   }
  // }

  // console.log("Usando for of 2");
  // // Destructuring, nodeJS v6+
  // for (var [index, user] of users.entries()) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of 3");
  // var index = 0;
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  //   index++;
  // }

  // console.log("Usando Array ForEach");
  // users.forEach(function (user, index) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //   }
  // });

  // console.log("Usando Array findIndex");
  // var indexOfElement = users.findIndex(
  //   function(element){
  //     console.log("comparando " + element.id + " y " +   req.params.id);
  //     return element.id == req.params.id
  //   }
  // )
  //
  // console.log("indexOfElement es " + indexOfElement);
  // if (indexOfElement >= 0) {
  //   users.splice(indexOfElement, 1);
  //   deleted = true;
  // }
}

//Con esto lo que hacemos es exponer las funciones del controlador
//para poder referenciarla en otros archivos
module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
